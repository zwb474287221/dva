import * as userservice from '../services/userlogin';

export default {

  namespace: 'userlogin',

  state: {},

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {  // eslint-disable-line
      console.log("fetch")
      let {data,headers} = yield call(userservice.query,{phone:'1000'});
      //let data = yield call(userservice.query);
      console.log(data)
      console.log(headers)
      yield put({ type: 'save',data:data});
    },
  },

  reducers: {
    save(state, action) {
      console.log(action)
      return { ...state, ...action.payload,...action.data };
    },
  },

};
